# Description

Deploy a Tensorflow Application (Object Detection Algorithm) with Docker on AWS SageMaker.

# Technology

* Python
* Scikit-learn
* Tensorflow
* Docker
* AWS SageMaker
